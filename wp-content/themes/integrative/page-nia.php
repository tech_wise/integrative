<?php get_header();  ?>
     <!-- NIA page starts -->
    <section class="nia">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-11">
            <div class="row">
              <div class="col-lg-5 text-lg-right text-center nia-img">
                <?php the_post_thumbnail();?>
              </div>
              <div class="col-lg-7 text-lg-left text-center px-lg-5 nia-text">
                 <?php the_content(); ?>
    
                <a href="<?php the_field('learn_more_link')?>" target="_blank"> Learn More</a>
    
                <a href="<?php the_field('dab_link')?>" target="_blank"> Division of Aging Biology (DAB)</a>
    
                <a href="<?php the_field('dgcg_link')?>" target="_blank">Division of Geriatrics and Clinical Gerontology (DGCG)</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- NIA page section end -->
<?php get_footer(); ?> 

