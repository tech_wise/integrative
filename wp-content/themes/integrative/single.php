<?php

get_header();
?>
<section class="inner-banner-1">
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-center">
	                    <h1><?php the_title(); ?></h1>
	                </div>
	            </div>
	        </div>
	    </div>
  	</section> 
<div class="container">


         <?php
          // / Start the Loop /
		  while ( have_posts() ) : the_post(); 
		  ?>
	         <div class="row inner-page">
				<!--<div class="col-md-12">-->
				<!--	<h1 class='page-title'> <?php the_title(); ?></h1>-->
				<!-- </div>-->
	            <div class="col-md-6">
	                <h1 class='page-title'> <?php the_title(); ?></h1>
   		            <?php the_content(); ?>
   		            <p><?php the_field('video_description');?></p>
                </div>
                <div class="col-md-6">
				
				    <?php 	if ( is_singular( 'slider-video' ) ) { ?>
                        
                        <div class="video slider">
                            <?php if( get_field('video_link') ): ?>
						         <!--video code cart-->
    				            <?php 	$video_link = explode('watch?v=', get_field('video_link')); ?>
                                <iframe width="500" height="300" src="https://www.youtube.com/embed/<?php echo ($video_link[1]); ?>" 
                                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                                allowfullscreen></iframe>
					        <?php endif; ?>
                        </div>
                        
                    <?php  }
                    elseif ( is_singular( 'research-staff' ) ){ ?>
				            <div class="staff-img">
				                	<?php the_post_thumbnail('large'); ?>
				            </div>
					<?php } 
					 elseif ( is_singular( 'about-investigator' ) ){ ?>
				            <div class="staff-img">
				                	<?php the_post_thumbnail('large'); ?>
				            </div>
				    	<?php } 
					 elseif ( is_singular( 'participatingsite' ) ){ ?>
				           <img src="<?php the_field('slider_big_image'); ?>" alt="" /> 
					<?php }else{
					    the_post_thumbnail('large');
					} ?>
                </div>
				 <span class="extra">
				  <?php the_field('extra_text');?>
				 </span>
            </div>
          <?php endwhile; // End of the loop.
          ?>
		

	</div><!-- #primary -->


<?php
get_footer();
