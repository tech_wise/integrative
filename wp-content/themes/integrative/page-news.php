<?php get_header();  ?> 
   <!-- first news section start -->
   <section class="main-news">
		<div class="container">
			<div class="row">
				<?php $args = array(  
						'post_type' => 'news_publication',
						'post_status' => 'publish',
						 'posts_per_page' => 1,
						'meta_query' => array(
							array(

								'key' => 'recent',
							    'value' => 'recent',
								'compare' => 'LIKE'
							)
						)
					);
				$loop = new WP_Query( $args ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
			    ?>
			   		<div class="col-lg-6 order-lg-first order-last new-left-box">
					<div class="row justify-content-between">
						<div class="col-5 col-md-6 date">
							<p><?php echo get_the_date('d/m/Y'); ?></p>
						</div>
						<div class="col-7 col-md-6 viewss  text-right">
							<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/view.png">-->
							<!--<a href="#">10 view</a>-->
							<?php echo do_shortcode("[post-views]"); ?>
						</div>
					</div>
					<div class="row news-text">
						<div class="col-md-12">
							<h1><?php the_title(); ?></h1>
							<p><?php the_content();?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 main-news-imag">
					<div class="news-img">
						<?php the_post_thumbnail( );?>
					</div>
					<div class="social-links">
					    <?php echo do_shortcode('[Sassy_Social_Share]'); ?>
						<!--<a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-icon.png"> </a>-->
						<!--<a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.png">       </a>-->
						<!--<a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/linked-icon.png">   </a>-->
						<!--<a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/share-icon.png">    </a>-->
					</div>
				</div>
				
			</div>
				<?php endwhile; 
				wp_reset_postdata(); ?>
		</div>
	   </div>
	</section>
	<!-- first news sedtion end -->
  <!-- first news sedtion end -->
  <!-- section recent post start -->
  <section class="recent-post">
	  <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h5>Recent Post</h5>
				</div>
		    </div>
		  	<div class="row recent-news">
				<?php $args = array(  
					'post_type' => 'news_publication',
					'post_status' => 'publish',
					'posts_per_page' => '3',
					'order' => 'dsc',
					'meta_query' => array(
							array(

								'key' => 'recent',
							    'value' => 'recent',
								'compare' => 'NOT LIKE'
							)
						)
				);
				$loop = new WP_Query( $args ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
			    ?>
					<div class="col-md-4 recent-card">
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
						<img src="<?php echo $url ?>" />
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/recentnew-curve.png" class="rnews-border">
						<h3><?php the_title(); ?></h3>
						<?php
                            $article_data = substr(get_the_content(), 0, 140);
                                
                            ?>
						<p><?php echo $article_data; ?> </p>
						<!--<p><?php the_content(); ?> </p>-->
						<a href="<?php the_permalink();?>">Read More <img src="<?php bloginfo('stylesheet_directory'); ?>/images/link-arrow.png"> </a>
					</div>
				<?php endwhile; 
				wp_reset_postdata(); ?>
			</div>
	  </div>
  </section>
  <!-- section recent post end -->
  <section class="publications">
		<div class="container">
			<div class="row">
				<div class="col-md-12 heading text-center">
					<h1><?php the_field('publication_section_heading'); ?></h1>
				</div>
			</div>
			<div class="row justify-content-center box-1">
				<div class="col-md-11">
					<div class="block-1">
						<div class="row justify-content-center">
							<div class="col-11">
								<h1><?php the_field('publication_box_1_title'); ?></h1>
								<p><?php the_field('publication_box_1_description'); ?></p>
								<div class="row left-text">
									<div class="col-sm-8">
										<p><span><?php the_field('publication_box_1_description_2'); ?></span></p>
									</div>
									<div class="col-sm-4 right-butn text-right">
										<a href="<?php the_field('publication_box_1_link');?> " target="_blank">Publications <img src="<?php bloginfo('stylesheet_directory'); ?>/images/block-arrow.png"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center box-2">
				<div class="col-md-11">
					<div class="block-2">
						<div class="row justify-content-center">
							<div class="col-11">
								<div class="row">
									<div class="col-md-9">
										<h3><?php the_field('publication_box_2_title'); ?></h3>
										<h5><?php the_field('publication_box_2_author'); ?></h5>
										<p><?php the_field('publication_box_2_description'); ?></p>
									</div>
									<div class="col-md-3 text-right book-img">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/agelater.jpg">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
<?php get_footer(); ?> 