<?php get_header();  ?>
 <?php   
   $page_id = 13;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
$content = apply_filters('the_content', $page_data->post_content);
   ?>
   
	
	<!-- Study Purpose Section Starts -->
	<section class="study-purpose">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="our-study-image">
						<img src="<?php the_field('our_study_purpose_image',13); ?>" alt="Old Woman">
					</div>
					<?php the_field('our_study_purpose_text',13) ?>
				
				
					<div class="learn-more-link">
						<a href="<?php echo get_bloginfo('url')?>/profile-in-human">Learn More</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Study Purpose Section Ends -->
	
	<!-- Our Investigators Section Starts -->
	<section class="investigators">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-7 col-md-9 heading text-center">
					<?php the_field('words_from_heading',13); ?>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div id="investigator-slider" class="carousel slide carousel-multi-item v-2" data-ride="carousel" data-interval="3000000">
			
						<div class="carousel-inner v-2" role="listbox">
							
							<?php $count=1; ?>
							<?php $args = array(  
								'post_type' => 'slider-video',
								'post_status' => 'publish',
								'order' => 'DSC',
							);
						
							$loop = new WP_Query( $args ); 
							while ( $loop->have_posts() ) : $loop->the_post(); 
							?>
							<div class="carousel-item">
								<div class="col-md col-11 investigator">
									<div class="video">
										<?php 
								// 		the_content();
										?>
										<?php
								 		$video_link = explode('watch?v=', get_field('video_link')); 

										?>
                                        <iframe width="250" height="230" src="https://www.youtube.com/embed/<?php echo ($video_link[1]); ?>" 
                                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                                        allowfullscreen></iframe>
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/play.png" class="play-btn">
									</div>
									
									<div class="text">
										<h3><?php the_title();?></h3>
										<p><?php the_field('video_description');?></p>
									</div>
								</div>
							</div>
						
							<?php $count++;?>
							<?php endwhile; 
							wp_reset_postdata(); ?>
							


						</div>
						<!--Controls-->
						<a class="carousel-control-prev" href="#investigator-slider" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next text-faded" href="#investigator-slider" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	
	</section>
	<!-- Our Investigators Section Ends -->

	<!--section about us start  -->
	<section class="about-us">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 heading text-center">
					<h2>About Us</h2>
				</div>
			</div>
			<div class="row justify-content-center">

			    <?php $args = array(  
					'post_type' => 'about-investigator',
					'post_status' => 'publish',
				
					'orderby' => 'post_date',
				        
					'meta_query' => array(
						array(
						    
						    'key'   => 'home',
							'value' => 'home',
							'compare' => 'LIKE'
						)
					)
				);
				$loop = new WP_Query( $args ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
			    ?>
			    <div class="about-box col-12 col-md-6 col-lg-4">
				  <div class="card mb-2">
				    <?php the_post_thumbnail();?>
					<div class="card-body">
						<h4><?php the_title(); ?></h4>
						<?php the_content(); ?>
					</div>
				  </div>
				</div>
				<?php endwhile; 
				wp_reset_postdata(); ?>
			
				</div>
				<!--Controls-->
			<div class="row justify-content-center">
				<div class="col-md-12 text-center about-btn">
					<a href="<?php echo get_bloginfo('url');?>/about-us/#nav-investigator" class="btn-about">View All Team Members</a>
				</div>
			</div>
		</div>
	</section>
	<!-- section about us end -->

	<!-- Non-Human Species Section Start -->
	<section class="non-human">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-7 col-md-9 text-center heading">
					<!--<h2>Profiles in Non-Human Species</h2>-->
					<?php the_field('profile_non_human_heading',13) ?>
				</div>
				<div class="col-md-12 graph text-center">
					<img src="<?php the_field('profile_non_human_image',13) ?>">
					<h3><?php the_field('non_human_title_under_image',13) ?></h3>
					<a href="<?php echo get_bloginfo('url');?>/profile-in-non-human-specie" class="profile-btn">Learn More</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Non-Human Species Section End -->

	<!-- Our Participants Section Start -->
	<section class="participants home">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 text-center heading">
					<h2><?php the_field('our_participants_heading',13); ?></h2>
				</div>
				<div class="col-md-12">
					<?php $count=1 ?>
						<?php $args = array(
												'post_type' => 'our-participant-1',
												'post_status' => 'publish',
												'posts_per_page' => -1,
												'order' => 'DSC',
						);

						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
							<?php if($count==5){?>
							<div class="item item-<?php echo $count; ?> show">
							<?php }
									else{?>
							<div class="item item-<?php echo $count; ?>">
							<?php }?>
								<div class="filter filter-<?php echo $count; ?>" style="background-image:url('<?php echo $url; ?>');background-repeat:no-repeat">
									<h3><?php the_title();?><br><span>Year</span></h3>
									<h4><?php the_field('year');?></h4>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line">
								</div>
							</div>
						<?php $count++; ?>
						<?php endwhile;  wp_reset_postdata(); ?>
				</div>
				<div class="col-md-6 col-6 prev button text-right">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-button.png">
				</div>
				<div class="col-md-6 col-6 button next">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-button.png">
				</div>
				<div class="col-md-12 text-center">
					<a href="<?php echo get_bloginfo('url') ?>/longevity" class="btn-profiles">Profiles in Human</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Our Participants Section End -->

	<!-- Recent News Section Start  -->
	<section class="recent-news">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 text-center heading">
					<h1><?php the_field('recent_news_heading',13); ?></h1>
				</div>
			</div>
			<div class="row">
				<?php $args = array(  
					'post_type' => 'news_publication',
					'post_status' => 'publish',
					'posts_per_page' => '3',
					'order' => 'dsc',
					'meta_query' => array(
							array(

								'key' => 'recent',
							    'value' => 'recent',
								'compare' => 'NOT LIKE'
							)
						)
				);
				$loop = new WP_Query( $args ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
			    ?>
					<div class="col-md-4 recent-card">
						<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
						<img src="<?php echo $url ?>" />
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/recentnew-curve.png" class="rnews-border">
						<h3><?php the_title(); ?></h3>
						<?php
                            $article_data = substr(get_the_content(), 0, 140);
                                
                            ?>
						<p><?php echo $article_data; ?> </p>
						<a href="<?php the_permalink();?>">Read More <img src="<?php bloginfo('stylesheet_directory'); ?>/images/link-arrow.png"> </a>
					</div>
					<?php endwhile; 
					wp_reset_postdata(); ?>

			</div>
			<div class="row justify-content-center">
				<div class="col-md-4 text-center view-btn">
					<a href="<?php echo get_bloginfo('url') ?>/news">View All</a>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent News Section End -->
	
<?php get_footer(); ?> 