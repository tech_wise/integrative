<?php get_header();  ?>


  <!-- Enroll study text Section Starts -->
  <section class="enroll-study">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 enroll-text">
                <?php the_content(); ?>
            </div>
        </div>
      <!--  Enroll form start-->
        <div class="row justify-content-center">
            <div class="mt-5 enroll-form-area col-md-10">
				<?php echo do_shortcode( '[contact-form-7 id="331" title="Enroll Form"]' ); ?>

<!--                 <form action="#" class="enroll-form" >
                        <div class="input-field col-md-6">
                            <div class="form-group form-group-sm">
                                <input type="text" class="form-control row enroll-form-input" placeholder="First Name">
                            </div>
                        </div>
                        <div class="input-field col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control row enroll-form-input"  placeholder="Last Name">
                            </div>
                        </div>
                        <div class="input-field col-md-6">
                        <div class="form-group form-group-sm">
                            <input type="text" class="form-control row enroll-form-input" placeholder="Phone Number">
                        </div>
                        </div>
                        <div class="input-field col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control row enroll-form-input"  placeholder="Email">
                        </div>
                        </div>
    
    
                        <div class="input-field col-xl-12 form-group">
                            <input type="text" class="form-control row enroll-form-input"  placeholder="Mailing address (optional)">
                        </div>
    
                        <div class="input-field col-xl-12">
                            <textarea class="form-control row enroll-form-input" rows="2"  placeholder="Your message"></textarea>
                        </div>
                        <div class="input-field submit-btn col-md-12">
                            <a href="#">Submit</a>
                        </div>
                </form> -->
                <!-- enroll form end -->
            </div>
        </div>
    </div>
  </section>
  <!--  Enroll study text section End -->

  <!-- Address section start -->
  <section class="address">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6 addres-box left-address">
                        <img src="<?php the_field('left-box_img'); ?>">
                        <?php the_field('left-box_heading'); ?>
                        <ul>
							<?php
                              $link = get_field('left-box_cellphone');
                              $link2=get_field('right-box_cellphone');
                              $email1 = get_field('left-box_email');
                              $email2 = get_field('right-box_email');
                              $right=substr($email2, 7);							
                              $left=substr($email1, 7); 							
                            ?>
                            <li><a href="<?php echo $link['url']; ?>"><?php echo  $link["title"]?></a>                                 </li>
							<li><a href="<?php the_field('left-box_email'); ?>"> <?php echo $left; ?></a>                             </li>
                        </ul>
                    </div>
                    <div class="col-md-6 justify-content-center mx-auto  addres-box right-address">
                        <div class="second-address">
					        <img src="<?php the_field('right-box_img'); ?>">
					        <?php the_field('right-box_heading'); ?>
					        <ul>
								<li><a href="<?php echo $link2['url']; ?>"> <?php echo  $link2["title"]?>                                 </a></li>
						        <li><a href="<?php the_field('right-box_email'); ?>"><?php echo $right; ?>                                 </a></li>
					        </ul>
				        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  <!-- Address section end -->

<?php get_footer(); ?> 