<?php get_header();  ?>

    <!-- Collaboraion Section Starts -->
   <section class="collaboraion">
     <div class="container collaboration-box">

       <!-- Family Study portion-->
       <div class="row family-study justify-content-end">
           <div class="col-lg-5 col-md-6 family-study-text">
              <?php the_field('box_1_text'); ?>
           </div>
          <div class="col-md-6 text-center family-study-img">
             <img src="<?php the_field('box_1_image'); ?>" />
          </div>
       </div>

       <!-- Longevity  Consertium portion-->
        <div  class="row consortium justify-content-start">
          <div class="col-md-6 text-center order-md-1 order-2  consortium-img">
           <img src="<?php the_field('box_2_image'); ?>" />
          </div>
          <div class="col-lg-5 col-md-6 order-md-2 order-1 consortium-text">
              <?php the_field('box_2_text'); ?>
          </div>
        </div>
         <!-- Longevity Genomics portion-->
         <div class="row genomics justify-content-end">
          <div class="col-md-6 genomics-text">
              <?php the_field('box_3_text'); ?>
          </div>
          <div class="col-md-6 text-center genomics-img">
           <img src="<?php the_field('box_3_image'); ?>" />
          </div>
         </div>
       </div>
   </section>
    <!--  Collaboration section End -->
<?php get_footer(); ?> 