<?php get_header();  ?>
    <!-- Model Section Starts -->
    <section class="model">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading text-center">
                    <h2><?php the_field('model_section_heading');?> </h2>
                    <h5><?php the_field('model_section_sub_heading');?> </h5>
                </div>
            </div>
            <div class="row details justify-content-center">
                <div class="col-lg-7 text">
                    <p><?php the_content(); ?></p>
                </div>
                <div class="col-lg-5 col-md-8 investigator-profile">
                    <?php 
// 					the_field('video'); 
					$video_link = explode('watch?v=', get_field('video')); 
				?>
                 <iframe width="100%" height="300px" src="https://www.youtube.com/embed/<?php echo ($video_link[1]); ?>" 
 frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"  allowfullscreen></iframe>
					
                   
                    <h5><?php the_field('investigator_name');?> </h5>
                    <p><?php the_field('investigator_description');?></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Model Section End -->

	<!-- Analysis Section Starts -->
	<section class="analysis">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h5><?php the_field('analysis_section_heading');?></h5>
					<img src="<?php the_field('analysis_section_image');?>" alt="Analysis">
				</div>
			</div>
		</div>
	</section>
	<!--- Analysis Section Ends --->
<?php get_footer(); ?> 
