<?php get_header();  ?>

    <!-- Tabs -->
    <section class="tabs">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <nav>
                                <div class="nav nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-participate-tab" data-toggle="tab" href="#nav-participate" role="tab" aria-controls="nav-home" aria-selected="true"><span><?php the_field('first_tab_heading')?></span></a>
                                    <a class="nav-item nav-link" id="nav-investigator-tab" data-toggle="tab" href="#nav-investigator" role="tab" aria-controls="nav-profile" aria-selected="false"><span><?php the_field('second_tab_heading')?></span></a>
                                    <a class="nav-item nav-link" id="nav-research-tab" data-toggle="tab" href="#nav-research" role="tab" aria-controls="nav-contact" aria-selected="false"><span><?php the_field('third_tab_heading')?></span></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <!-- Participant Sites Tab Starts -->
                        <div class="tab-pane fade text-center show active nav-participate" id="nav-participate" role="tabpanel" aria-labelledby="nav-participate-tab">
                            <h2><?php the_field('first_tab_heading')?></h2>
                            <?php $count=0 ?>
                            <div class="row">
                            <ol class="carousel-indicators-custm">
                                <?php $args = array(
                                                        'post_type' => 'participatingsite',
                                                        'post_status' => 'publish',
                                                        'posts_per_page' => -1,
                                                        'order' => 'ASC',
                                                       
                                );
    
                                $loop = new WP_Query( $args );
                                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                 <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $count; ?>" class="active">
                                    <?php 
									 the_post_thumbnail(); ?>
                                </li>
                                <?php $count++; ?>
                                <?php endwhile;  wp_reset_postdata(); ?>
                              </ol>                       
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-11 text-left">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <!-- The slideshow -->
                                    <?php $number=1; ?>
                                    <div class="carousel-inner">
                                    <?php $args = array(
                                                        'post_type' => 'participatingsite',
                                                        'post_status' => 'publish',
                                                        
                                                        
                                                        'order' => 'ASC',
                                    );
    
                                    $loop = new WP_Query( $args );
                                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <?php if($number==1){?>
                                    <div class="carousel-item active" id="item<?php echo $number;?>">
                                            <div class="row">
                                                <div class="col-lg-4 slider-logo text-center">
                                                     <?php 
// 														 the_post_thumbnail();?> 
											 
												<img src="<?php the_field('slider_big_image'); ?>" alt="" /> 
                                                </div>
                                                <div class="col-lg-8 description">
                                                    <h4><?php the_title(); ?></h4>
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }else{?>
                                    <div class="carousel-item" id="item<?php echo $number;?>">
                                            <div class="row">
                                                <div class="col-lg-4 slider-logo text-center">
                                                     <?php 
// 												the_post_thumbnail(); 
// 												?>
													<img src="<?php the_field('slider_big_image'); ?>" alt="" />
                                                </div>
                                                <div class="col-lg-8 description">
                                                    <h4><?php the_title(); ?></h4>
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                    <?php $number++; ?>
                                    <?php endwhile;  wp_reset_postdata(); ?>
                                    </div>
                                
                                    <!--Carousel Controls-->
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                        <!-- Participant Sites Tab Starts -->

                        <!-- Investigator Tab Starts -->
                        <div class="tab-pane fade text-center nav-investigator" id="nav-investigator" role="tabpanel" aria-labelledby="nav-investigator-tab">
                            <h2><?php the_field('second_tab_heading')?></h2>
                            <div class="row justify-content-start">
                                
                               
                                <?php $args = array(
                                                        'post_type' => 'about-investigator',
                                                        'post_status' => 'publish',
                                                        'posts_per_page' => -1,
                                                        'order' => 'DSC',
                                );

                                $loop = new WP_Query( $args );
                                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                
                                 <div class="col-lg-4 col-md-6 investigator">
									<div class="img-border">
										<a href="<?php the_field('image_link'); ?>" target="_blank">
											<?php  the_post_thumbnail('large'); ?>
										</a>
									 </div>
                                    <h6><?php the_title() ;?></h6>
                                    <!--<p>Contact PI</p>-->
                                    <!--<p>Director of the New England Centenarian Study, -->
                                    <!--    Professor of Medicine at Boston University</p>-->
                                    <!--<p>Geriatrics, extreme longevity, genetics</p>-->
                                    <?php the_content(); ?>
                                </div>
                                
                                <?php endwhile;  wp_reset_postdata(); ?>
                                
                            </div>
                        </div>
                        <!-- Investigator Tab Starts -->

                        <!-- Research Staff Tab Starts -->
                        <div class="tab-pane fade text-center nav-research" id="nav-research" role="tabpanel" aria-labelledby="nav-research-tab">
                            <h2><?php the_field('third_tab_heading')?></h2>
                            <div class="row">
                                <?php $args = array(
                                                        'post_type' => 'research-staff',
                                                        'post_status' => 'publish',
                                                        'posts_per_page' => -1,
                                                        'order' => 'DSC',
                                );

                                $loop = new WP_Query( $args );
                                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                
                                   
                                <div class="col-lg-3 col-md-4 staff-member">
                                    <div class="img-border">
                                        <?php the_post_thumbnail( );?>
                                    </div>
                                    <h5><?php the_title(); ?></h5>
                                    <?php the_content(); ?>
                                </div>
                                <?php endwhile;  wp_reset_postdata(); ?>

                        </div>
                        <!--- Research Staff Tab Ends --->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Tabs -->
<?php get_footer(); ?> 
