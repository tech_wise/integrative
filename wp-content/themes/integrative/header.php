<!DOCTYPE html>
<html lang="en">
<head>
    
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="<?php bloginfo('stylesheet_directory'); ?>//style.css" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/compiled-addons-4.19.1.min.css" rel="stylesheet"/>
	<link href="<?php bloginfo('stylesheet_directory'); ?>/css/mdb.min.css" rel="stylesheet"/>
    <link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/images/logo-icon.png"/>

    <?php wp_head(); ?>
</head>

<!--facebook share button jqery end-->
<body <?php body_class(); ?>
 <!-- Header Starts -->
	<header>
	    <div class="container">
	        <div class="row justify-content-between">
	            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-4 col-4 logo">
	                <a href="<?php echo get_bloginfo('url')?>">
	                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png" alt="Logo">
	                </a>
	            </div>
	            <div class="col-xl-10 col-lg-4 col-md-9 col-sm-8 col-8 header-nav">
	                <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	               
	                <ul class="search-and-btn">
	                   
	                    <li class="active">
						<img class="fa fa-search" aria-hidden="true" src="<?php bloginfo('stylesheet_directory'); ?>/images/search-icon.png" alt="Search-Icon">
    						<!-- <i class="fa fa-search" aria-hidden="true">  </i> -->
							<form action="<?php echo get_site_url(); ?>" method="get">
							<div class="search-box">	
									<input type="text" class="search-bar" name="s" placeholder="Search"/>
									 <input type="submit" class="search-btn" value="Search"/>
								
								</div>
							</form>
    					</li>
	                    <li>
	                        <a href="enroll">Enroll in the Study</a>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>
	</header>
	<!-- Header Ends -->
	<!--Banner section start-->
<?php   
    if(is_front_page()){
        get_template_part('template-parts/main-banner');
    } 
    else{
        get_template_part('template-parts/inner-banner');
    }
?>
	<!--Banner section end-->
