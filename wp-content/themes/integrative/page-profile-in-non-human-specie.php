<?php get_header();  ?>
    <!-- Profile in Non-human species -->
    <section class="nonhuman-specie">
      <div class="container">
        <div class="text-center mb-5"><h2><?php the_field('section_heading'); ?></h2></div>
        <div class="row">
          <div class="col-lg-9 order-lg-1 order-2 text-left text-1">
            <p><?php the_field('box-1_text'); ?></p>
          </div>
          <div class="col-lg-3 text-lg-left text-center animal-img order-lg-2 order-1">
            <img src="<?php the_field('box-1_img'); ?>"  alt="rat" />
          </div>
        </div>
        <div class="row mt-lg-5 nonhuman-birds">
          <div class="col-lg-4 animal-img text-lg-left text-center">
            <img src="<?php the_field('box-2_img'); ?>" alt="" />
          </div>
          <div class="col-lg-8 text-left text-2">
            <p><?php the_field('box-2_text'); ?></p>
          </div>
        </div>
        <div class="row mt-lg-5 nonhuman-skunk">
          <div class="col-lg-9 order-lg-1 order-2 text-3 text-left">
            <p><?php the_field('box-3_text'); ?></p>
          </div>
          <div class="col-lg-3 order-lg-2 order-1 text-lg-left text-center">
            <img src="<?php the_field('box-3_img'); ?>" alt="" />
          </div>
        </div>
        <div class="row mt-lg-5 nonhuman-pygmy">
          <div class="col-lg-4 text-lg-left text-center">
            <img src="<?php the_field('box-4_img'); ?>" alt="" />
          </div>
          <div class="col-lg-8 text-4 text-left">
            <p><?php the_field('box-4_text'); ?></p>
          </div>
        </div>
      </div>
    </section>

    <!-- Profile in Non-human graph -->
    <section class="profile-non-human">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-6 col-lg-7 col-md-9 text-center heading">
            <?php the_field('profile_non_human_heading',13) ?>
          </div>
          <div class="col-md-12 graph text-center">
             <img src="<?php the_field('profile_non_human_image',13) ?>">
			 <h3><?php the_field('non_human_title_under_image',13) ?></h3>
          </div>
        </div>
      </div>
    </section>
   <!-- Profile in Non-human species ends -->
<?php get_footer(); ?> 

