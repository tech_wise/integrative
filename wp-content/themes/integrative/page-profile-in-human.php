<?php get_header();  ?>
    <!-- Profile in human starts -->
    <section class="profile-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-left profile-text">
             <?php the_content(); ?>
            <a class="profile-btn1" href="<?php echo bloginfo('url'); ?>/enroll">Enroll in the Study </a>
            <a class="profile-btn2 ml-3" href="<?php echo bloginfo('url'); ?>/collaboration">View Collaborations</a>
          </div>
          <div class="col-lg-6 text-center pl-lg-5 profile-card">
           <?php 
// 			  the_field('investigator_video'); 
				$video_link = explode('watch?v=', get_field('investigator_video')); 
				?>
                 <iframe width="100%" height="300px" src="https://www.youtube.com/embed/<?php echo ($video_link[1]); ?>" 
 frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"  allowfullscreen></iframe>
			 
            <!--<video-->
            <!--  src=""-->
            <!--  type="video/mp4"-->
            <!--  poster="<?php bloginfo('stylesheet_directory'); ?>/images/ips-profile.png"-->
            <!-->
            <!--</video>-->
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/play.png" alt="" />
            <h4><?php the_field('investigator_name')?></h4>
            <p><?php the_field('investigator_description')?></p>
            <div class="row mt-5 justify-content-around">
              <div class="col-5">
                <img src="<?php the_field('institute_logo_1')?>" alt="" />
              </div>
              <div class="col-5">
                <img src="<?php the_field('institute_logo_2')?>" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Profile in human end -->
<?php get_footer(); ?> 

