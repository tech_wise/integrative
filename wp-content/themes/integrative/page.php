<?php get_header(); ?>


<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="banner-text text-center">
        <h2><?php echo the_title(); ?></h2>
      <p> <?php the_field('inner_banner_text');?></p>
      </div>
    </div>

  </div>
</div>


<div class="container my-5">
  <div class="row">
    <div class="col-12">
            <?php
			while ( have_posts() ) : the_post();
			
			the_content();


			endwhile; // End of the loop.
			?>         
    </div>
  </div>
</div>


<?php get_footer(); ?> 