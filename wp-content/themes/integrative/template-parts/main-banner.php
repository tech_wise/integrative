<!-- Banner Starts -->
	<section class="main-banner">
	    <div class="banner-image">
	        <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/banner-img.png" alt="Banner Image">-->
	        <?php echo get_the_post_thumbnail(13, 'full'); ?>
	    </div>
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-xl-5 col-md-6 col-sm-5 col-6 text text-center">
	                    <?php the_field('banner_text',13); ?>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
<!-- Banner Ends -->
