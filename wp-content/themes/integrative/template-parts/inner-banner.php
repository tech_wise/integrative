    <!-- Banner Starts -->
<?php if(is_page('29')){ ?>
    <section class="inner-banner-2">
        <div class="banner-image">
	        <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/ips-banner.png" alt="Banner Image">-->
	        <?php the_post_thumbnail( );?>
	    </div>
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-center">
	                    <h1><?php the_title(); ?></h1>
	                </div>
	            </div>
	        </div>
	    </div>
    </section>
<?php }
elseif(is_page('21')){ ?>
    <section class="inner-banner-5">
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-md-8 text text-center">
						<h1><?php the_title(); ?></h1>
                  </div>
                  <div class="col-md-4 baner-dog-img">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/dog.png">
                  </div>
	            </div>
	        </div>
	    </div>
  	</section>
<?php }
elseif(is_page('23')){ ?>
    <section class="inner-banner-2">
        <div class="banner-image">
	        <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/news-banner.png" alt="Banner Image">-->
	        <?php the_post_thumbnail( );?>
	    </div>
	    <div class="banner-text news-bnr">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-left">
	                    <h1><?php the_title(); ?></h1>
	                </div>
	            </div>
	        </div>
	    </div>
    </section>
<?php }
elseif(is_page('25')){ ?>
    <section class="inner-banner-1 nia-banner">
      <div class="banner-text">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 text text-center">
              <h1><?php the_title(); ?></h1>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php }
elseif(is_page()){ ?>
<section class="inner-banner-1">
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-center">
	                    <h1><?php the_title(); ?></h1>
	                </div>
	            </div>
	        </div>
	    </div>
  	</section> 
<?php } ?>
    
    <!--- Banner Ends --->