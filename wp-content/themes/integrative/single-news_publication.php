<?php get_header(); ?>
 <?php   
   $page_id = 23;  //Page ID
$page_data = get_page( $page_id ); 

//store page title and content in variables
$title = $page_data->post_title; 
 ?>
    <section class="inner-banner-2">
        <div class="banner-image">
	        <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/news-banner.png" alt="Banner Image">-->
	       <?php echo get_the_post_thumbnail(23, 'full'); ?>
	    </div>
	    <div class="banner-text news-bnr">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-left">
	                    <h1><?php echo $title; ?></h1>
	                </div>
	            </div>
	        </div>
	    </div>
    </section>
<div class="container my-5">
  <div class="row detail-page-news">
    
        
            <?php
			while ( have_posts() ) : the_post(); ?>
			
		    <div class="col-lg-6 new-left-box">
				<div class="row justify-content-between main-news">
					<div class="col-5 col-md-6 date">
						<p><?php echo get_the_date('d/m/Y'); ?></p>
					</div>
					<div class="col-7 col-md-6 viewss  text-right">
						<!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/view.png">-->
						<!--<a href="#">10 view</a>-->
						<?php echo do_shortcode("[post-views]"); ?>
					</div>
				</div>
				<div class="row news-text detail-text">
					<div class="col-md-12">
					    <h1><?php the_title(); ?></h1>
					    <p><?php the_content();?></p>
					</div>
				</div>
			</div>
			<div class="col-lg-6 main-news-imag">
				<div class="news-img detail-page">
					<?php the_post_thumbnail( );?>
				</div>
				<div class="social-links">
				    	<?php echo do_shortcode('[Sassy_Social_Share]'); ?>
				    
					<!--<a href="#">-->
					<!--    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook-icon.png">-->
					<!--</a>-->
					<!--<a href="https://twitter.com/share" class="twitter-share-button" data-via="smashingmag" data-text="<?php the_title(); ?>" data-url="<?php the_permalink(); ?>">-->
					<!--    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/twitter.png">      -->
					<!--</a>-->
					<!--<a href="#">-->
					<!--    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/linked-icon.png">  -->
					<!--</a>-->
					<!--<a href="#">-->
					<!--    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/share-icon.png">    -->
					<!--</a>-->
				</div>
			
			
				</div>
		
			<div class="row">
				<div class="col-md-12 detail-extra-text">
				    <p><?php the_field('detail_description') ;?></P>
				</div>
			</div>
				
<?php
			endwhile; // End of the loop.
			?>         

  </div>
</div>


<?php get_footer(); ?> 