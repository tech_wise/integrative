	<!-- footer start -->
	<footer>
		<div class="container">
			<div class="row justify-content-center footer-text">
				<div class="col-md-4 footer-logo text-right">
					<a href="<?php get_bloginfo('url')?>">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.png">
					</a>
					<span class="right-border-line"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/right-border.png"></span>
				</div>
				<div class="col-md-7">
					<?php dynamic_sidebar( 'footer-text-widget' ); ?>
				</div>
			</div>
			<div class="footer-nav">
				<div class="row justify-content-center">
					<div class="col-xl-10">
					    <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
					
					</div>
				</div>
				<hr>
				<div class="row copy-right">
					<div class="col-md-12 text-center">
				        <?php dynamic_sidebar( 'copyright-widget' ); ?>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer end -->

    <!--video slider jquery start-->
    <script>
        jQuery(document).ready(function(){
	        jQuery('.carousel-item:first-child').addClass('active');
	   });	
	
    </script>
<!--video slider jquer end-->

	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery-3.4.1.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
	<!-- slider js  link-->
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/compiled-addons.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/mdb.min.js"></script>
<?php wp_footer();?>
</body>
</html>