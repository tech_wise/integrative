<?php
get_header();
?>
<section class="inner-banner-1">
	    <div class="banner-text">
	        <div class="container">
	            <div class="row justify-content-center">
	                <div class="col-12 text text-center">
                        <h1 style="font-size:25px;">Search Result:"<span class="page-description"><?php echo get_search_query(); ?></span>"</h1>
                        
                        
	                </div>
	            </div>
	        </div>
	    </div>
 </section> 
	<section class="inner search-inner">
		<div class="container">
			<div class="row">
				<div class="col-md-12 py-4">
				<?php
                         if ( have_posts() ) : 
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						
						echo "<br>";
						echo '<h2> '.the_title().'</h2>';
						echo '<p>'.the_excerpt().'</p>';
						
						echo'<div class="read-more">';
						echo '<a href="'.get_permalink().'"> Read More</a>';
						echo"</div>";
						echo "<br>";
						echo "<hr>";
						
						

					endwhile; // End of the loop.
					else :
					  	echo'<div class="col-md-12 not-find">';
						echo '<h2>Result Not Find</h2>';
						echo"</div>";
	                	endif;
				?>
				</div>
			</div>
		</div>			

	</section>
	
<?php
get_footer();
?>
