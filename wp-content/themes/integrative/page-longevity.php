<?php get_header();  ?>    
    <!---Longevity in human page starts--->

    <!-- Our Participants Section Start -->
    <section class="participants jane-doe">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 text-center heading">
            <h2><?php the_field('participant_1_heading');?></h2>
          </div>
          <div class="col-md-12">
			  <?php $count=1 ?>
					<?php $args = array(
											'post_type' => 'our-participant-1',
											'post_status' => 'publish',
											'posts_per_page' => -1,
											'order' => 'DSC',
					);

					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
						<?php if($count==5){?>
							<div class="item item-<?php echo $count; ?> show">
							<?php }
									else{?>
							<div class="item item-<?php echo $count; ?>">
							<?php }?>
							<div class="filter filter-<?php echo $count; ?>" style="background-image:url('<?php echo $url; ?>');background-repeat:no-repeat">
								<h3><?php the_title();?><br><span>Year</span></h3>
								<h4><?php the_field('year')?></h4>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png">
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line">
							</div>
						</div>
					<?php $count++; ?>
					<?php endwhile;  wp_reset_postdata(); ?>
<!--             <div class="item item-1">
              <div class="filter filter-1">
                <h3>13<br /><span>Year</span></h3>
                <h4>1928</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-2">
              <div class="filter filter-2">
                <h3>16<br /><span>Year</span></h3>
                <h4>1931</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-3">
              <div class="filter filter-3">
                <h3>30<br /><span>Year</span></h3>
                <h4>1945</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-4">
              <div class="filter filter-4">
                <h3>38<br /><span>Year</span></h3>
                <h4>1953</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-5 show">
              <div class="filter filter-5">
                <h3>44<br /><span>Year</span></h3>
                <h4>1959</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-6">
              <div class="filter filter-6">
                <h3>61<br /><span>Year</span></h3>
                <h4>1976</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-7">
              <div class="filter filter-7">
                <h3>68<br /><span>Year</span></h3>
                <h4>1983</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-8">
              <div class="filter filter-8">
                <h3>79<br /><span>Year</span></h3>
                <h4>1994</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-9">
              <div class="filter filter-9">
                <h3>105<br /><span>Year</span></h3>
                <h4>2020</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div> -->
          </div>
          <div class="col-md-6 col-6 prev button text-right">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-button.png" />
          </div>
          <div class="col-md-6 col-6 button next">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-button.png" />
          </div>
          <div class="col-md-8 text-center">
            <p><?php the_field('participant_1_description');?></p>
          </div>
        </div>
      </div>
    </section>

    <section class="participants ann-doe">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 text-center heading">
            <h2><?php the_field('participant_2_heading');?></h2>
          </div>
          <div class="col-md-12 text-center">
			  <?php $count=1 ?>
				<?php $args = array(
					'post_type' => 'our-participant-2',
					'post_status' => 'publish',
					'posts_per_page' => -1,
					'order' => 'DSC',
				);

				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
					<?php if($count==5){?>
							<div class="item item-<?php echo $count; ?> show">
							<?php }
									else{?>
							<div class="item item-<?php echo $count; ?>">
							<?php }?>
						<div class="filter filter-<?php echo $count; ?>" style="background-image:url('<?php echo $url; ?>');background-repeat:no-repeat">
							<h3><?php the_title();?><br><span>Year</span></h3>
							<h4><?php the_field('year')?></h4>
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line">
						</div>
					</div>
				<?php $count++; ?>
				<?php endwhile;  wp_reset_postdata(); ?>
<!--             <div class="item item-1">
              <div class="filter filter-1">
                <h3>13<br /><span>Year</span></h3>
                <h4>1927</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-2">
              <div class="filter filter-2">
                <h3>16<br /><span>Year</span></h3>
                <h4>1930</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-3">
              <div class="filter filter-3">
                <h3>25<br /><span>Year</span></h3>
                <h4>1939</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-4">
              <div class="filter filter-4">
                <h3>31<br /><span>Year</span></h3>
                <h4>1945</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-5 show">
              <div class="filter filter-5">
                <h3>37<br /><span>Year</span></h3>
                <h4>1951</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-6">
              <div class="filter filter-6">
                <h3>49<br /><span>Year</span></h3>
                <h4>1963</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-7">
              <div class="filter filter-7">
                <h3>63<br /><span>Year</span></h3>
                <h4>1977</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div>
            <div class="item item-8">
              <div class="filter filter-8">
                <h3>106<br /><span>Year</span></h3>
                <h4>2020</h4>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/participants-border.png" />
                <img src="<?php bloginfo('stylesheet_directory'); ?>/images/line.png" class="line" />
              </div>
            </div> -->
          </div>
          <div class="col-md-6 col-6 prev button text-right">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/prev-button.png" />
          </div>
          <div class="col-md-6 col-6 button next">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/next-button.png" />
          </div>
          <div class="col-md-8 text-center">
            <p><?php the_field('participant_2_description'); ?></p>
          </div>
        </div>
      </div>
    </section>
    <!-- Our Participants Section End -->
    <!---Longevity in human page ends--->
<?php get_footer(); ?> 
