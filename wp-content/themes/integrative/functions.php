<?php

/**
 * integrative functions and definitions
 *
 * @package integrative
 * @since integrative 1.0
 */


if (!function_exists('integrative_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     */
    function integrative_setup(){

             /**
         * Make theme available for translation.
         * Translations can be placed in the /languages/ directory.
         */
        load_theme_textdomain( 'integrative', get_template_directory() . '/languages' );
     
        /**
         * Add default posts and comments RSS feed links to <head>.
         */
        add_theme_support( 'automatic-feed-links' );
     
        /**
         * Enable support for post thumbnails and featured images.
         */
        add_theme_support( 'post-thumbnails' );
        //page title function  
         add_theme_support('title-tag');


        /**
         * Add support for two custom navigation menus.
         */
        register_nav_menus(array(
            'primary'   => __('Primary Menu', 'integrative'),
            'secondary' => __('Secondary Menu', 'integrative')
           
        ));


        function integrative_widgets_init()
        {
            
            register_sidebar(array(
                'id' => 'footer-text-widget',
                'name' => 'Footer Text widget',
                'description' => 'Widget area for short description in footer',
                'before_widget' => '',
                'after_widget' => '',
                'before_title' => '',
                'after_title' => ''
            ));

            register_sidebar(array(
                'id' => 'copyright-widget',
                'name' => 'Copy Right Widget',
                'description' => 'Widget area for social icons in footer',
                'before_widget' => '',
                'after_widget' => '',
                'before_title' => '',
                'after_title' => ''
            ));
			
		}
        add_action('widgets_init', 'integrative_widgets_init');



        /**
         * Enable support for the following post formats:
         * aside, gallery, quote, image, and video
         */
        //add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
    }
endif;
add_action('after_setup_theme', 'integrative_setup');



